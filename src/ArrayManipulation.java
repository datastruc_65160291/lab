public class ArrayManipulation {
    public static void main(String[] args) throws Exception {
        int numbers[] = {5, 8, 3, 2, 7};
        String names[] = {"Alice", "Bob", "Charlie", "David"};
        double values[] = new double[4];

        for(int i = 0; i < 5 ; i++){
            System.out.print(numbers[i] + " ");
        }
        System.out.println("");

        for (String i : names) {
            System.out.print(i + " ");
        }
        System.out.println("");
        
        values[0] = 1.1;
        values[1] = 1.2;
        values[2] = 1.3;
        values[3] = 1.4;

        int sumnumbers = 0;
        for(int i = 0; i < 5 ; i++){
            sumnumbers = sumnumbers + numbers[i];
        }
        System.out.println(sumnumbers);

        double resulvalues = 0;
        for(int i = 0 ;i<4;i++){
            if(values[i] >= resulvalues){
                resulvalues = values[i];
            }
        }
        System.out.println(resulvalues);

        int cal = 4;
        String reversedNames[] = new String[4];
        for (String i : names) {
            cal = cal - 1;
            reversedNames[cal] = i;
        }

        for (String i : reversedNames) {
            System.out.print(i + " ");
        }

    }
}
